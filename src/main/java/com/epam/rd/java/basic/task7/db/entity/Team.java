package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;
	private String prevName;
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getPrevName() {
		return prevName;
	}
	public void setName(String name) {
		this.prevName=this.name;
		this.name = name;
	}

	public static Team createTeam(String name) {
		Team team = new Team();
		team.setName(name);
		team.setId(0);
		return team;
	}
	@Override
	public String toString() {
		return getName();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Team team = (Team) o;
		return name.equals(team.name);
	}


}
