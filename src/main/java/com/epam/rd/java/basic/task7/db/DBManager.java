package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	private static DBManager instance;
	private static Connection connection;

	public static synchronized DBManager getInstance() {
		if (instance==null) return new DBManager();
		return instance;

	}

	private DBManager() {
		try {
			if (connection!=null) connection.close();
			Properties properties = new Properties();
			FileInputStream fis = new FileInputStream("app.properties");
			properties.load(fis);
			connection=DriverManager.getConnection(properties.getProperty("connection.url"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		instance=this;
	}

	public List<User> findAllUsers() throws DBException {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM USERS");
			List<User> result = new ArrayList<>();
			while(resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt(1));
				user.setLogin(resultSet.getString(2));
				result.add(user);
			}
			return result;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e.getCause());
		}
	}

	public boolean insertUser(User user) throws DBException {
		Statement statement = null;
		try {
			statement = connection.createStatement();
			statement.execute("INSERT INTO USERS (LOGIN) VALUES ('"+user.getLogin()+"')");
		} catch (SQLException e) {
			return false;
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		var ref = new Object() {
			boolean result = true;
		};
		Arrays.stream(users).forEach(user -> {
			ref.result = ref.result && deleteUser(user);
		});
		return ref.result;
	}

	private boolean deleteUser(User user) {
		Statement statement = null;
		try {
			statement = connection.createStatement();
			statement.execute("DELETE FROM USERS WHERE LOGIN = "+user.getLogin());
		} catch (SQLException e) {
			return false;
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM USERS WHERE LOGIN =\'"+login+"\'");
			while(resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt(1));
				user.setLogin(resultSet.getString(2));
				return user;
			}
			return null;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e.getCause());
		}
	}

	public Team getTeam(String name) throws DBException {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM TEAMS WHERE NAME =\'"+name+"\'");
			while(resultSet.next()) {
				Team team = new Team();
				team.setId(resultSet.getInt(1));
				team.setName(resultSet.getString(2));
				return team;
			}
			return null;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e.getCause());
		}
	}

	public List<Team> findAllTeams() throws DBException {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM TEAMS");
			List<Team> result = new ArrayList<>();
			while(resultSet.next()) {
				Team team = new Team();
				team.setId(resultSet.getInt(1));
				team.setName(resultSet.getString(2));
				result.add(team);
			}
			return result;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e.getCause());
		}
	}

	public boolean insertTeam(Team team) throws DBException {
		Statement statement = null;
		try {
			statement = connection.createStatement();
			statement.execute("INSERT INTO TEAMS (NAME) VALUES('"+team.getName()+"')");
		} catch (SQLException e) {
			return false;
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Statement statement = null;
		user = getUser(user.getLogin());
		try {
			connection.setAutoCommit(false);
			connection.setSavepoint();
			statement = connection.createStatement();
			for (Team team : teams) {
				if(team!=null) {
					team = getTeam(team.getName());
					statement.execute("INSERT INTO USERS_TEAMS (USER_ID, TEAM_ID) VALUES("+user.getId()+","+team.getId()+")");
				}
			}
			connection.commit();
			connection.setAutoCommit(true);
			return true;
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException ex) {
				throw new DBException(e.getMessage(),e.getCause());
			}
			throw new DBException(e.getMessage(),e.getCause());
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		Statement statement = null;
		user = getUser(user.getLogin());
		try {
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM USERS_TEAMS WHERE USER_ID ="+user.getId());
			List<Team> teams = new ArrayList<>();
			while(resultSet.next()) {
				statement = connection.createStatement();
				ResultSet teamName = statement.executeQuery("SELECT * FROM TEAMS WHERE ID="+resultSet.getInt(2));
				while (teamName.next()) {
					Team team = new Team();
					team.setId(teamName.getInt(1));
					team.setName(teamName.getString(2));
					teams.add(team);
				}
			}
			return teams;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
		Statement statement = null;
		team = getTeam(team.getName());
		try {
			statement = connection.createStatement();
			statement.execute("DELETE FROM TEAMS WHERE ID = "+team.getId());
		} catch (SQLException e) {
			return false;
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		Statement statement = null;
		Team prevTeam = getTeam(team.getPrevName());
		try {
			statement = connection.createStatement();
			statement.execute("UPDATE TEAMS SET NAME = '"+team.getName()+"' WHERE ID = "+prevTeam.getId());
		} catch (SQLException e) {
			return false;
		}
		return true;
	}

}
